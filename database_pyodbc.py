# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 16:24:34 2021

@author: BenNorris
"""

import pyodbc
import pandas as pd
import os
import numpy as np
import re
import datetime as dt
#%%
df=pd.read_pickle(os.path.join(os.getcwd(), 'full_table_new'))
#df['creation_date']=pd.to_datetime(df['creation_date'], format='%Y-%m-%d %H:%M:%S')
columns_sql=list(df.columns)
for i in range(len(columns_sql)):
    columns_sql[i] = columns_sql[i].replace(' ','_')
columns_sql = tuple(columns_sql)
datatypes_sql=[str(i) for i in df.dtypes.values]

for i in range(len(datatypes_sql)):
    if datatypes_sql[i]=='float64':
        datatypes_sql[i]='FLOAT'
    elif datatypes_sql[i]=='int32':
        datatypes_sql[i]='INT'
    elif datatypes_sql[i]=='int64':
        datatypes_sql[i]='INT'
    elif datatypes_sql[i]=='datetime64[ns]':
        datatypes_sql[i]='DATE'
    elif datatypes_sql[i]=='object':
        datatypes_sql[i]='DATE'
        
dict_sql=dict(zip(columns_sql, datatypes_sql))

columns_and_types=[]
for key, value in dict_sql.items():
    column_and_type=f'{key} {value}'
    columns_and_types.append(column_and_type)
    
columns_and_types=', '.join(columns_and_types)

        
#%%Defining the sql server.
server='localhost'
database='master'
conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; \
                    SERVER=' + server + '; \
                    DATABASE=' + database +';\
                    Trusted_Connection=yes;')

cursor=conn.cursor()


     
rows_to_insert = df.values
tuples_to_insert=[]
    
for i in rows_to_insert:
    new_row = [str(e) for e in i]
    new_row = ((tuple(list(new_row))))
    tuples_to_insert.append(new_row)
tuples_to_insert = tuple(tuples_to_insert)


for i in tuples_to_insert:
    try:
        insert_query = f'''INSERT INTO bank_churn_db1 VALUES {i}'''
        cursor.execute(insert_query)
        conn.commit()
    except Exception:
        pass
        
query='''SELECT * FROM bank_churn_db1'''
cursor.execute(query)
for row in cursor:
    print(row)
cursor.close()









