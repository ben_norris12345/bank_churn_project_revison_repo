# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 16:49:06 2021

@author: BenNorris
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt
from sklearn.utils import resample as resample
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import accuracy_score, roc_auc_score, roc_curve, plot_roc_curve, confusion_matrix
#%%
df=pd.read_pickle(os.path.join(os.getcwd(), 'full_table_new'))
#%%

def rel_change_creator(df, month_rel_changes):
    for i in month_rel_changes:
        #for a difference sicne three months previosuly. Precentage of current balance
        df['rel_change_in_deposit_amt']=df.groupby('customer_id')['deposit'].diff(i)/df['amt_in_account']
        df['rel_change_in_withdrawal_amt']=df.groupby('customer_id')['withdrawal'].diff(i)/df['amt_in_account']
        #relative chnage in the amount in their account
        df['rel_change_in_amt_in_account']=df.groupby('customer_id')['amt_in_account'].diff(i)/df['amt_in_account']
        return df
df_new=rel_change_creator(df,[1])

#%%
#adding a columns with total number of deposits for each customer. 

df_no_trans=df[['customer_id', 'date', 'count_of_deposits_per_month','count_of_withdrawal_per_month']].groupby(['customer_id']).cumsum().rename(columns={'count_of_deposits_per_month':'running_sum_of_deposits','count_of_withdrawal_per_month':'running_sum_of_withdrawals'})
df_new=pd.concat([df_new, df_no_trans], axis=1)


#%%dropping the nan values from rel cahnges and dropping bad columns
df_new.dropna(axis=0, inplace=True)
df_new.drop(columns=['account_id','dob','is_0'], inplace=True)
df=df_new
#%%resampling the dataframe
df_churners=df[df['churn_in_next_month']==1]
df_non_churners=df[df['churn_in_next_month']==0]

def resampler(df_churners, df_non_churners):
    df_non_churners=resample(df_non_churners, replace=False, n_samples=len(df_churners))
    df_equal=pd.concat([df_non_churners, df_churners], axis=0)
    return df_equal

df=resampler(df_churners, df_non_churners)
#%%
#filling in nans with zeros for ustoemrs only holding account for one month.
df.replace(np.inf, np.nan, inplace=True)
df.replace(-np.inf, np.nan, inplace=True)
df.fillna(0, inplace=True)

#%%Saving the final dataframe
filepath_merged_table=os.path.join(os.getcwd(), 'full_table_new')
pd.to_pickle(df, filepath_merged_table)

#%%Initisl model fitting
df_train, df_test=train_test_split(df, test_size=0.2, random_state=123, shuffle=True)
features=df.drop(columns=['customer_id', 'date', 'churn_in_next_month', 'creation_date','max_date_account_open']).columns
X_train=df_train[features]
y_train=df_train['churn_in_next_month']
model=RandomForestClassifier()
model.fit(X_train, y_train)
X_test=df_test[features]
y_test=df_test['churn_in_next_month']
y_hat=model.predict(X_test)
y_hat_probs=model.predict_proba(X_test)
#%%Evaluation
acc=accuracy_score(y_hat, y_test)
roc=roc_auc_score(y_hat, y_test)
fpr = roc_curve(y_test, y_hat_probs[:,1])[0]
tpr = roc_curve(y_test, y_hat_probs[:,1])[1]
#param_grid={'n_estimators':[30,50,100], 'min_samples_split':[0.5, 1, 2], 'criterion':['gini','entropy']}

fig, ax=plt.subplots(figsize=(12, 6))
ax.plot([0,1],[0,1], linestyle='--', color='red')
#plot_roc_curve(model, X_test, y_test, name='ROC curve for Random Forest Classifier', ax=ax)
ax.plot(fpr, tpr, label="RF Classifier ROC AUC: {:.2f}".format(roc))
ax.legend()
#%%Tuning hyperparametrs
param_grid={'n_estimators':[30,50,100], 'min_samples_split':[0.5, 1, 2], 'criterion':['gini','entropy']}
#param_grid={'n_estimators':[30], 'min_samples_split':[0.5], 'criterion':['gini']}

grid_search=GridSearchCV(estimator=model, param_grid=param_grid, cv=5)
grid_search.fit(X_train, y_train)
#%%
opt_hyperparams=grid_search.best_params_

model_opt=RandomForestClassifier(criterion='entropy',min_samples_split=2, n_estimators=100)
model_opt.fit(X_train, y_train)
y_hat_opt=model_opt.predict(X_test)
y_hat_opt_probs=model_opt.predict_proba(X_test)
#%%
roc_opt=roc_auc_score(y_hat_opt, y_test)
#%%

conf_matrix=confusion_matrix(y_test, y_hat)
#%%





















