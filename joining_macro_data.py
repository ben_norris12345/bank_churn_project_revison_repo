# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 17:01:48 2021

@author: BenNorris
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt
#%%
folder_filepath=os.path.join(os.getcwd(),'macroeconomic_data')

#%%
'''
for file in os.listdir(folder_filepath)[0:2]:
    df_macro=pd.read_csv(os.path.join(folder_filepath,file))
    df_macro.rename(columns={'DATE':'date'}, inplace=True)
    df_macro['date']=pd.to_datetime(pd.to_datetime(df_macro['date']).apply(lambda x: x.strftime('%Y-%m-%d')))
    df=pd.merge(df,df_macro, how='left', on='date')
'''
df=pd.read_csv(os.path.join(folder_filepath, 'Fedrates.csv'))
gdp=pd.read_csv(os.path.join(folder_filepath, 'GDPPC.csv'))
df=pd.merge(df, gdp, how='left', on='DATE')
industrial_product=pd.read_csv(os.path.join(folder_filepath, 'IndustrialProduct.csv'))
df=pd.merge(df, industrial_product, how='left', on='DATE')
inflation=pd.read_csv(os.path.join(folder_filepath, 'Inflation.csv'))
df=pd.merge(df, inflation, how='left', on='DATE')
psr=pd.read_csv(os.path.join(folder_filepath, 'PersonalSavingsRate.csv'))
df=pd.merge(df, psr, how='left', on='DATE')
treasury=pd.read_csv(os.path.join(folder_filepath, 'Treasury10yRate.csv'))
df=pd.merge(df, treasury, how='left', on='DATE')
unemployment=pd.read_csv(os.path.join(folder_filepath, 'Unemployment.csv'))
df=pd.merge(df, unemployment, how='left', on='DATE')
USEuroExchangeRate=pd.read_csv(os.path.join(folder_filepath, 'USEuroExchangeRate.csv'))
df=pd.merge(df, USEuroExchangeRate, how='left', on='DATE')

#Filling in the missing values.
df['GDPC1']=df['GDPC1'].interpolate(method='linear')
df['GDPC1']=df['GDPC1'].fillna(method='bfill')
df['INDPRO']=df['INDPRO'].interpolate(method='linear')
df['INDPRO']=df['INDPRO'].fillna(method='bfill')
df['DGS10']=df['DGS10'].interpolate(method='linear')
#Filling in the full stops.
df.loc[(df['T10YIE']=='.'), 'T10YIE']=np.nan
df['PSAVERT']=df['PSAVERT'].astype('float').interpolate(method='linear')
df.loc[(df['PSAVERT']=='.'), 'PSAVERT']=np.nan
df['PSAVERT']=df['PSAVERT'].fillna(method='bfill')
df['T10YIE']=df['T10YIE'].astype('float').interpolate(method='linear')

df.loc[(df['DGS10']=='.'), 'DGS10']=np.nan
df['DGS10']=df['DGS10'].astype('float').interpolate(method='linear')
df['DGS10']=df['DGS10'].fillna(method='bfill')

df.loc[(df['UNRATE']=='.'), 'UNRATE']=np.nan
df['UNRATE']=df['UNRATE'].astype('float').interpolate(method='linear')
df['UNRATE']=df['UNRATE'].fillna(method='bfill')

df.loc[(df['DEXUSEU']=='.'), 'DEXUSEU']=np.nan
df['DEXUSEU']=df['DEXUSEU'].astype('float').interpolate(method='linear')
df['DEXUSEU']=df['DEXUSEU'].fillna(method='bfill')

#%%Changing date column
df.rename(columns={'DATE':'date'}, inplace=True)
df['date']=pd.to_datetime(pd.to_datetime(df['date']).apply(lambda x: x.strftime('%Y-%m-%d')))

#%%Merging with the whole dataframe

bank_df=pd.read_pickle(os.path.join(os.getcwd(), 'merged_table'))
full_df=pd.merge(bank_df, df, how='left', on='date')

#%%Saving the final dataframe

full_df.to_pickle(os.path.join(os.getcwd(), 'full_table.pickle'))
#also save as a CSV
full_df.to_csv(os.path.join(os.getcwd(), 'full_table.csv'))