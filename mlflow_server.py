# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 11:42:26 2021

@author: BenNorris
"""
#%%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt

from sklearn.utils import resample as resample
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.inspection import permutation_importance
from sklearn.metrics import accuracy_score, roc_auc_score, roc_curve, plot_roc_curve, confusion_matrix
import mlflow

import json
import mlflow
import pandas as pd
import waitress
import requests
from flask import Flask, request

#%%
best_models_folder=r'C:/Users/BenNorris/Documents/bank_churn_revision/best_models'
model_uri='file://' + os.path.join(best_models_folder, 'RF_feature_scanner')
model=mlflow.sklearn.load_model(model_uri)

#%%
app=Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    print('route: /')

@app.route('/echo', methods=['GET','POST'])
def echo():
    if request.method == 'GET':
        print('route: /echo (GET)')
    if request.method == 'POST':
        print('route: /echo (POST)')
    return request.data

@app.route('/invocations', methods=['POST'])
def predict():
    '''
    Data comes in in json format X dataframe
    '''
    X_df=pd.DataFrame(json.loads(request.data))
    #print(request.data)
    y_hat=model.predict(X_df).tolist()
    y_hat_prob=model.predict_proba(X_df)[:,1].tolist()
    response=json.dumps(y_hat)
    response_prob=json.dumps(y_hat_prob)
    return response

waitress.serve(app, host='0.0.0.0', port=1338)
        






