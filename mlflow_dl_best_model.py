# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 10:47:42 2021

@author: BenNorris
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt

from sklearn.utils import resample as resample
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.inspection import permutation_importance
from sklearn.metrics import accuracy_score, roc_auc_score, roc_curve, plot_roc_curve, confusion_matrix
import mlflow

#%%
experiment_name='bank_churn_random_forest_classifier'
tracking_uri=r'file:C:/Users/BenNorris/Documents/bank_churn_revision/mlflow_runs/mlruns'
destination_folder=r'C:/Users/BenNorris/Documents/bank_churn_revision/best_models'

client=mlflow.tracking.MlflowClient(tracking_uri=tracking_uri)
experiment_id=client.get_experiment_by_name(experiment_name).experiment_id
runs=client.search_runs(experiment_id, order_by=['metrics.ROC_AUC'])
best_run_id=runs[-1].info.run_id

client.download_artifacts(best_run_id, 'RF_feature_scanner', destination_folder)
#%%Opening the model
model_uri='file://' + os.path.join(destination_folder, 'RF_feature_scanner')
model=mlflow.sklearn.load_model(model_uri)

#%%Visulaising all the run into a dataframe
df_runs=pd.DataFrame([{**i.data.metrics, **i.data.params, **dict(i.info)} for i in runs])
#%%Using model to make predictions
print(model.get_params())
#%%Testing the model

df=pd.read_pickle(os.path.join(os.getcwd(), 'full_table_new'))
df_train, df_test=train_test_split(df, test_size=0.2, random_state=123, shuffle=True)
y_train=df_train['churn_in_next_month']
y_test=df_test['churn_in_next_month']

#%%Testing the best model
X_df_indices=df_test[labels[:24]].sample(2000).index
X_df=df_test.loc[X_df_indices, labels[:24]]
y_actual=df_test.loc[X_df_indices, 'churn_in_next_month']
X_dict=json.dumps(X_df.to_dict())
y_hat=model.predict(pd.DataFrame(json.loads(X_dict))).tolist()
y_hat_prob_positive=model.predict_proba(pd.DataFrame(json.loads(X_dict)))[:,1].tolist()
roc=roc_auc_score(y_actual, y_hat_prob_positive)
