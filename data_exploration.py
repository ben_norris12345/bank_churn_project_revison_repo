# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 16:58:11 2021

@author: BenNorris
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt
#%%
df=pd.read_pickle(os.path.join(os.getcwd(), 'full_table.pickle'))
#df['churned_account']=df['churned_account'].apply(lambda x: 1 if x==True else 0)
#%%Scatter matricx
scatter_features=['deposit','withdrawal','start_balance','amt_in_account','age','DFF','GDPC1']
df_scatter=df[scatter_features]
pd.plotting.scatter_matrix(df_scatter, alpha=0.2)

#%%State demographic
#Proportion of people who have churned belonging to each state
df_state_population=df[['state','customer_id']].groupby('state').nunique()#.rename(columns={'churn_in_next_month':'no_of_churners'})
df_state_churners=df[['state','churn_in_next_month']].groupby('state').sum()
num_of_customers= df[['customer_id']].nunique().values[0]
df_state=df_state_churners/num_of_customers

fig, ax =plt.subplots()
df_state.plot(ax=ax)
ax.set_title('Plot show the proprtion of churners belongig to each state')

#%%Histogram of the age of customers
fig, ax=plt.subplots()
df_ages=df[['age']].hist(bins=30, ax=ax, density=True)

#%%Plot the churn rate vs the year
df['year']=pd.DatetimeIndex (df['date']).year
df_churning_by_year=df[['churn_in_next_month','year']].groupby('year').sum()
number_of_churners=len(df[df['churn_in_next_month']==1])
df_no_customer_in_year=df[['year','customer_id']].groupby('year').nunique().rename(columns={'customer_id':'no_customers_with_bank'})

df_churn_rate_per_year=df_churning_by_year['churn_in_next_month'].div(df_no_customer_in_year['no_customers_with_bank']).rename({'0':'proportion_of_customers_who_leave_bank'})

fig, ax=plt.subplots()
df_churn_rate_per_year.plot(kind='bar',ax=ax)
ax.set_title('churn rate as a fucntion of year')

#%%One hot encoding, dropping unimportant features and renaming columns
df=pd.get_dummies(df, prefix='is', columns=['state'], dtype=int)
#%%
df.rename(columns={'DFF':'FedRates','GDPC1':'GDP','INDPRO':'IndustrialProduct','T10YIE':'Inflation','PSAVERT':'PersonalSavings','DGS10':'TreasuryRate','UNRATE':'UnemploymentRate','DEXUSEU':'USDEUROexchangerate'}, inplace=True)
filepath_merged_table=os.path.join(os.getcwd(), 'full_table_new')
pd.to_pickle(df, filepath_merged_table)

#%%Plotting the correlations of wach feature
corr_matrix=df.corr()
df_correlation=pd.DataFrame(df.corr().values)
df_correlation.columns=df.columns
fig, ax =plt.subplots()
ax.imshow(corr_matrix)
ax.set_xticklabels=df_correlation.columns
ax.imshow(df_correlation)

