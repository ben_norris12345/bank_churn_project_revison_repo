# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 14:25:48 2021

@author: BenNorris
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt
from sklearn.utils import resample as resample
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.inspection import permutation_importance
from sklearn.metrics import accuracy_score, roc_auc_score, roc_curve, plot_roc_curve, confusion_matrix
import mlflow
import torch
import torch.nn as nn
from torch_lr_finder import LRFinder


def evaluator(y_test, y_hat, y_hat_probs):
    roc=roc_auc_score(y_test, y_hat_probs[:,1])
    acc=accuracy_score(y_test, y_hat)
    return roc, acc
#%%

df=pd.read_pickle(os.path.join(os.getcwd(), 'full_table_new'))
df_train, df_test=train_test_split(df, test_size=0.2, random_state=123, shuffle=True)
features=['rel_change_in_withdrawal_amt','rel_change_in_deposit_amt', 'FedRates', 'amt_in_account', 'rel_change_in_amt_in_account', 'GDP','UnemploymentRate','age','amount','is_California','is_Texas','running_sum_of_withdrawals','running_sum_of_deposits','start_balance','USDEUROexchangerate','IndustrialProduct','withdrawal','Inflation','PersonalSavings','deposit','TreasuryRate','year','count_of_withdrawal_per_month','is_Florida','is_New York']

device=torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

class DataSet(torch.utils.data.Dataset):
    def __init__(self, data, features, device=torch.device('cpu')):
        super().__init__()
        self.data=data
        self.device=device
        self.features=features
        
    def __getitem__(self, index):
        X = torch.tensor(self.data.iloc[index][features], dtype=torch.float32).to(device)
        y = torch.tensor(self.data.iloc[index][['churn_in_next_month']], dtype=torch.float32).to(device)
        return X, y
    
    def __len__(self):
        return len(self.data)
    
trainset=DataSet(df_train, features)
testset=DataSet(df_test, features)
trainloader=torch.utils.data.DataLoader(trainset, shuffle=True, batch_size=512)
testloader=torch.utils.data.DataLoader(testset, shuffle=True, batch_size=512)

#%%Defining the neeral network.

class NeuralNet(nn.Module):
    def __init__(self, n_in, n_out):
        super().__init__()
        self.fc1=nn.Linear(n_in, 128)
        self.fc2=nn.Linear(128, 64)
        self.fc3=nn.Linear(64, 32)
        self.fc4=nn.Linear(32, 32)
        self.fc5=nn.Linear(32,16)
        self.output_layer=nn.Linear(16, n_out)
        self.dropout=nn.Dropout(0.2)
        #self.sm=nn.Softmax(dim=1)
        
    def forward(self, x):
        x=self.fc1(x)
        x=nn.functional.relu(x)
        x=self.fc2(x)
        x=nn.functional.relu(x)
        x=self.dropout(x)
        x=self.fc3(x)
        x=nn.functional.relu(x)
        x=self.fc4(x)
        x=nn.functional.relu(x)
        x=self.fc5(x)
        x=nn.functional.relu(x)
        x=self.output_layer(x)
        x=self.dropout(x)
        return nn.Sigmoid()(x)
#BEST ACCURACY = 0.5761
#BEST CROSS ENTROPY = 0.6445

#%%COonv Net

    
n_epochs=6
model=NeuralNet(len(features), 1).to(device)
model=model.train()
optimizer=torch.optim.Adam(model.parameters(), lr=2.61E-04)
#criterion=nn.CrossEntropyLoss()
criterion=nn.BCELoss()
scheduler=torch.optim.lr_scheduler.StepLR(optimizer, 3e-5)
#%%
#scheduler=torch.optim.lr_scheduler.StepLR(optimizer, 0.0001)

#Training loop
for i in range(n_epochs):
    correct=0
    total=0
    for X, y in trainloader:
        output=model(X.float())
        loss=criterion(output, y)
        print('Binary Cross Entropy Loss:', loss.item())
        #print('Fraction of data:', len(y)/len(trainloader))
        #print(output)
        y_pred=torch.Tensor([int(1) if i[0]>0.5 else int(0) for i in output])
        model.zero_grad()
        loss.backward()
        optimizer.step()
        total+=len(y)
        correct+=(y_pred==y[:,0]).sum().item()
        
    print('Epoch Number:', i+1)
    print('Cross Entropy:', loss.item())
    accuracy=correct/float(total)
    print('Accuracy', accuracy)
    
#%%Testing the model
model.eval()

#%%
lr_finder=LRFinder(model, optimizer, criterion, device=device)
lr_finder.range_test(trainloader, start_lr=1e-8, end_lr=0.1, num_iter=200)
lr_finder.plot()
lr_finder.reset()
