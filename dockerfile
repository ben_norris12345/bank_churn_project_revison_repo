FROM dangawne/ubuntu_miniconda3x

COPY ./model_code /model_code

WORKDIR /model_code

RUN conda env create --file ./env_updated.yml

RUN conda env update --name venv_bank_churn --file ./RF_feature_scanner/conda.yaml

ENTRYPOINT ["conda", "run", "-n", "venv_bank_churn", "python", "./mlflow_server.py"]