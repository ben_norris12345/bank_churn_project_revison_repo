# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 12:21:27 2021

@author: BenNorris
"""
#%%
import pandas as pd

import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt

from sklearn.utils import resample as resample
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.inspection import permutation_importance
from sklearn.metrics import accuracy_score, roc_auc_score, roc_curve, plot_roc_curve, confusion_matrix
import mlflow

import json
import mlflow
import pandas as pd
import waitress

from flask import Flask, request
import requests
from sklearn.metrics import accuracy_score, roc_auc_score

df=pd.read_pickle(os.path.join(os.getcwd(), 'full_table_new'))
df_train, df_test=train_test_split(df, test_size=0.2, random_state=123, shuffle=True)
y_train=df_train['churn_in_next_month']
y_test=df_test['churn_in_next_month']

labels_file_path=r'C:/Users/BenNorris/Documents/bank_churn_revision/best_feature_labels/best_features.txt'
labels=[]
with open(labels_file_path, 'r') as f:
    for line in f:
        labels.append(line[:-1])
#%%
r1=requests.get('http://127.0.0.1:1337/echo') #http://127.0.0.1:1337/echo for local machine.
print('Status Code:', r1.status_code)

string='Test the post function works for echo'
r2=requests.post('http://127.0.0.1:1337/echo', data=string)
print('Status Code:', r2.status_code)
print(r2.content)
#%%Making predictions

            
X_df_indices=df_test[labels[:24]].sample(20000).index
X_df=df_test.loc[X_df_indices, labels[:24]]
y_actual=df_test.loc[X_df_indices, 'churn_in_next_month']
X_dict=json.dumps(X_df.to_dict())
r3 = requests.post('http://127.0.0.1:1337/invocations', data=X_dict)
y_pred=r3.content
#y_pred_proba=[float(i[:-1]) for i in r3.content.decode('utf-8')[1:-1].split()]
y_pred=[float(i[:-1]) for i in r3.content.decode('utf-8')[1:-1].split()]
#roc=roc_auc_score(y_actual, y_pred_proba)
print(y_pred)

