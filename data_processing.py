# -*- coding: utf-8 -*-
"""
Created on Sun Jan 10 13:04:32 2021

@author: BenNorris
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt
#%%Loading in the data.
filepath_customer=os.path.join(os.getcwd(),'customers_tm1_e.csv')
filepath_transactions=os.path.join(os.getcwd(),'transactions_tm1_e.csv')

customer_table=pd.read_csv(filepath_customer)
transactions_table=pd.read_csv(filepath_transactions)

#Aggregate the transaction_table. Drop the amount column.
transactions_table.drop(columns=['amount'], inplace=True)

customer_table=customer_table[customer_table['start_balance'] != -10000000000]
customer_table=customer_table[customer_table['start_balance'] != 1000000000]
customer_table=customer_table[customer_table['start_balance'] != 2000000000]

#%%
count_transactions=pd.DataFrame(transactions_table.groupby(['customer_id','account_id', 'date'], as_index=False).agg(np.count_nonzero))[['customer_id','deposit','withdrawal']]
count_transactions.rename(columns={'deposit':'count_of_deposits_per_month','withdrawal':'count_of_withdrawal_per_month'}, inplace=True)
#%%
agg_transactions=pd.DataFrame(transactions_table.groupby(['customer_id','account_id', 'date'], as_index=False).sum({'deposit':'sum_of_deposits', 'withdrawal':'sum_of_withdrawals'}))
agg_transactions=pd.concat([agg_transactions, count_transactions.drop(columns=['customer_id'])], axis=1)

#%%Adding in the start balance column
agg_transactions=agg_transactions.merge(customer_table[['customer_id','creation_date','start_balance']], how='left', left_on=['customer_id','date'], right_on=['customer_id','creation_date']).drop(columns=['creation_date'])
#Filling in the start_balance column
agg_transactions['start_balance']=agg_transactions['start_balance'].fillna(method='ffill')
#%%Getting a column with amount currently in account
agg_transactions['amount']=agg_transactions['deposit']+agg_transactions['withdrawal']
agg_transactions['amt_in_account']=agg_transactions.groupby('customer_id', as_index=False)['amount'].cumsum()
agg_transactions['amt_in_account']=agg_transactions['amt_in_account']+agg_transactions['start_balance']

#%%Join in all other data from customer table now we have aggregated.
df=agg_transactions.merge(customer_table[['customer_id','dob','state','creation_date']], how='left', on='customer_id')

#%%#Changing datetimes
df['date']=pd.to_datetime(df['date'], infer_datetime_format=True)
#df['dob']=pd.to_datetime(df['dob'], infer_datetime_format=True)
df['creation_date']=pd.to_datetime(df['creation_date'], infer_datetime_format=True)

#%%Adding in a chruned column. The date range is start_date='2007-01-31', end_date='2020-05-31'
max_date=pd.to_datetime('2020-05-31', infer_datetime_format=True)
max_date_account_is_open_df=df[['customer_id','date']].rename(columns={'date':'max_date_account_open'}).groupby('customer_id').max('max_date_account_open')

df=df.merge(max_date_account_is_open_df, on='customer_id', how='inner')
#%%

churn_df=df[['customer_id','date',]].groupby('customer_id').tail(1)
churn_df['churn_in_next_month']=np.ones(len(churn_df))
df=pd.merge(df, churn_df, how='left', on=['customer_id','date'])
df.fillna(0, inplace=True)
df.loc[df['date']==max_date, 'churn_in_next_month']=0
#replacing abnormal account balances with zero.
#%%replacing abnormal values with 0

df['amt_in_account']=df['amt_in_account'].apply(lambda x: 0 if x<0 or (x>0 and x<0.000001) else x)
#%%
#replacing states
df.loc[df['state']=='CALIFORNIA','state']='California'
df.loc[df['state']=='NY','state']='New York'
df.loc[df['state']=='MASS', 'state']='Massachusetts'
df.loc[df['state']=='TX', 'state']='Texas'

#%%
states_to_drop=df[(df['state']=='-999') | (df['state']=='UNK')].index
df=df.drop(states_to_drop)

#%%Adding in an age column
#df['dob']               = pd.to_datetime(df['dob'], format )
#df['creation_date']     = pd.to_datetime(df['creation_date'], format = '%Y-%m-%d').dt.date
#df['date']              = pd.to_datetime(df['date'], format = '%Y-%m-%d').dt.date

df_dob=pd.to_datetime(df[['dob']].stack(), infer_datetime_format=True).unstack()
df['age']=(df['date']-df_dob['dob']).astype('m8[Y]')
#%%Save to a pickle file
filepath_merged_table=os.path.join(os.getcwd(), 'merged_table')
pd.to_pickle(df, filepath_merged_table)


