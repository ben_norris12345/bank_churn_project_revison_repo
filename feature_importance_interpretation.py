# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 17:32:08 2021

@author: BenNorris
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import datetime as dt
from sklearn.utils import resample as resample
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.inspection import permutation_importance
from sklearn.metrics import accuracy_score, roc_auc_score, roc_curve, plot_roc_curve, confusion_matrix
from sklearn.preprocessing import StandardScaler
import mlflow
import random
labels_file_path=r'C:/Users/BenNorris/Documents/bank_churn_revision/best_feature_labels/best_features.txt'
labels=[]
with open(labels_file_path, 'r') as f:
    for line in f:
        labels.append(line[:-1])
#%%Mlflow ex[eriment name and tracking uri
experiment_name='bank_churn_random_forest_classifier_scaled'
tracking_uri=r'file:C:/Users/BenNorris/Documents/bank_churn_revision/mlflow_runs/mlruns'
mlflow.set_tracking_uri(tracking_uri)
mlflow.set_experiment(experiment_name)
#%%
df=pd.read_pickle(os.path.join(os.getcwd(), 'full_table_new'))
df_train, df_test=train_test_split(df, test_size=0.2, random_state=123, shuffle=True)
y_train=df_train['churn_in_next_month']
y_test=df_test['churn_in_next_month']

#%%

def model_fitter(model, df, features, *args, **kwargs):
    df_train, df_test=train_test_split(df, test_size=0.2, random_state=123, shuffle=True)
    scaler=StandardScaler()
    X_train=df_train[features]
    X_train=scaler.fit_transform(X_train)
    model.set_params(**kwargs)
    model.fit(X_train, y_train)
    X_test=df_test[features]
    X_test=scaler.fit_transform(X_test)
    y_hat=model.predict(X_test)
    y_hat_probs=model.predict_proba(X_test)
    return y_hat, y_hat_probs, model

def evaluator(y_test, y_hat, y_hat_probs):
    roc=roc_auc_score(y_test, y_hat_probs[:,1])
    acc=accuracy_score(y_test, y_hat)
    return roc, acc
#%%Benchamrk of using all the features

features_unreg=df.drop(columns=['customer_id', 'date', 'churn_in_next_month', 'creation_date','max_date_account_open']).columns
y_hat_unreg, y_hat_unreg_probs, model_unreg = model_fitter(RandomForestClassifier(), df, features_unreg, criterion='entropy', min_samples_split=2, n_estimators=100)
roc_unreg, acc_unreg = evaluator(y_test, y_hat_unreg, y_hat_unreg_probs)

#%%Plotting feature imprtances
'''
importances=permutation_importance(model_unreg, df_train[features_unreg], y_train)

#%%Plotting feature imprtances
importance_mean=np.sort(importances['importances_mean'])[::-1]
indices=np.argsort(importances['importances_mean'])[::-1]
labels=[features_unreg[i] for i in indices]
fig, ax = plt.subplots()
ax.bar(labels, importance_mean)
ax.set_xticklabels(labels, rotation=90)
'''
#%%
#save the most relevant labels as a txt file
labels_file_path=r'C:/Users/BenNorris/Documents/bank_churn_revision/best_feature_labels/best_features.txt'
with open(labels_file_path, 'w') as f:
    f.writelines('%s\n' % label for label in labels)
    f.close()
#%%Here we train a regualrized model only using the most rleevant fetaures. First 24 fetures


def mlflow_tracker(model, n_features, model_name, *args, **kwargs):

    with mlflow.start_run():
        features_reg=labels[0:n_features]
        y_hat, y_hat_probs, model=model_fitter(model, df, features_reg, **kwargs)
        mlflow.sklearn.log_model(model, model_name)
        roc, acc =  evaluator(y_test, y_hat, y_hat_probs)
        mlflow.log_metric('ROC_AUC', roc)
        mlflow.log_metric('Accuracy Score', acc)
        mlflow.log_param('Number of features', n_features)
        for key, value in kwargs.items():
            mlflow.log_param(key, value)
        return
mlflow_tracker(RandomForestClassifier(), 24, model_name='RF', criterion='entropy',min_samples_split=2, n_estimators=100)
#%%

def mlflow_rf_feature_iterator(n_features=[5]): #,10,15,20,24]):#n_features=np.arange(8,40,4)):
    model=RandomForestClassifier()
    for features in n_features:
        mlflow_tracker(model, features, model_name='RF_feature_scanner', criterion=random.choice(['entropy','gini']), min_samples_split=np.random.randint(2,5), n_estimators=np.random.randint(80,250))
    return
mlflow_rf_feature_iterator()

#%%Tracking on a remote server. Trackng the unreg model
'''
tracking_uri_remote = r'http://52.90.101.28:5000/'
experiment_name_remote='RF Unregularized Remote'
mlflow.set_tracking_uri(tracking_uri_remote)
mlflow.set_experiment(experiment_name_remote)

with mlflow.start_run():
    mlflow.sklearn.log_model(model_unreg, 'unregularized_model')
    mlflow.log_metric('ROC_unreg', roc_unreg)
    mlflow.log_metric('ACC_unreg', acc_unreg)
    for param_name, param_val in model_unreg.get_params():
        mlflow.log_param(param_name, param_val)
        
#%%
X_df=df_test[features_unreg]
y_actual=df_test['churn_in_next_month']
y_pred=model_unreg.predict(X_df)
y_pred_probs_positive=model_unreg.predict_proba(X_df)[:,1]
roc=roc_auc_score(y_actual, y_pred_probs_positive)
'''

    








